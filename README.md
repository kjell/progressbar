progressbar
===========


testing termui: https://github.com/gizak/termui

running:

```
# install go
 go get # to install deps.
 # test it:
 { (for i in $(seq 1 3 100 ); do echo $i; sleep 0.1; done;)& } | go run gauge.go
```
