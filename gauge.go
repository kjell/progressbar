package main

import "github.com/gizak/termui"
import tm "github.com/nsf/termbox-go"

//import "time"
//import "math/rand"
import "bufio"
import "strconv"
import "os"

/*
{ (for i in $(seq 1 3 100 ); do echo $i; sleep 0.1; done;)& } | go run gauge.go
{ (for i in $(seq 1 10 ); do echo $(((RANDOM % 100) + 1)); sleep 0.3; done;)& } | ./gauge
*/

func main() {
	err := termui.Init()
	if err != nil {
		panic(err)
	}
	defer termui.Close()
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		i, _ := strconv.Atoi(scanner.Text())
		update(i)
	}
	/*
		for i := 0; i <= 100; i++ {
			i = i + 4
			if i > 100 {
				i = 100
			}
			update(i)
			time.Sleep(time.Duration(rand.Intn(400)) * time.Millisecond)

		}
	*/
	termui.UseTheme("helloworld")
	tm.PollEvent()
}
func update(p int) {
	g0 := termui.NewGauge()
	g0.Percent = p
	g0.Width = 50
	g0.Height = 5
	g0.Border.Label = "Account status"
	if p > 70 && p < 90 {
		g0.BarColor = termui.ColorYellow
	} else if p >= 90 {
		g0.BarColor = termui.ColorGreen
	} else {
		g0.BarColor = termui.ColorRed
	}
	g0.Border.FgColor = termui.ColorWhite
	g0.Border.LabelFgColor = termui.ColorCyan

	termui.Render(g0)
}
